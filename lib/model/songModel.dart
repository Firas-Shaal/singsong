import 'package:get/get.dart';

class Song {
  Song({
    required this.title,
    required this.singer,
    required this.duration,
    required this.isPlayed,
  });

  late String title;
  late String singer;
  late String duration;
  RxBool isPlayed = false.obs;

  Song.fromJson(Map<String, dynamic> json) {
    title = json['title'];
    singer = json['singer'];
    duration = json['duration'];
    isPlayed.value = json['isPlayed'];
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['title'] = title;
    data['singer'] = singer;
    data['duration'] = duration;
    data['isPlayed'] = isPlayed.value;
    return data;
  }
}
