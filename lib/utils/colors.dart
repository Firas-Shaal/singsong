import 'package:flutter/material.dart';

const Color backgroundColor = Color(0xFF212A32);
const Color whiteColor = Color(0xFFFFFFFF);
const Color pinkColor = Color(0xFFF75191);
const Color shadowColorLight = Color(0xFF00000080);
const Color bottomBarColor = Color(0xFF303033);
const Color lightWhiteColor = Color(0xFFE4E8EB00);
const Color blackColor = Color(0xFF161A1A);
const Color purpleColor = Color(0xFF0022FF4D);
const Color inactiveColor = Color(0xFF909196);
