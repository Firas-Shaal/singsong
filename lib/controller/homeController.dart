import 'dart:async';
import 'dart:convert';

import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:music_app/model/songModel.dart';

class HomeController extends GetxController {
  var selectedIndex = 0.obs;

  RxList<Song> songs = <Song>[].obs;

  Rx<Song?> itemPLaying = Rx<Song?>(null);

  List<String> parts = [];
  int minutes = 0;
  int seconds = 0;

  RxInt currentDuration = 0.obs;
  RxInt totalDuration = 0.obs;
  Timer? timer;

  @override
  void onInit() {
    super.onInit();
    _loadSongs();
  }

  void isInit() {
    if (itemPLaying.value != null && timer == null) {
      currentDuration.value = 0;
      totalDuration.value = 0;
      parts = itemPLaying.value!.duration.split(":");
      minutes = int.parse(parts[0]);
      seconds = int.parse(parts[1]);
      totalDuration.value = minutes * 60 + seconds;
      itemPLaying.value!.isPlayed.value = true;
      timer = Timer.periodic(const Duration(seconds: 1), (timer) {
        if (itemPLaying.value != null) {
          if (currentDuration.value == totalDuration.value) {
            itemPLaying.value!.isPlayed.value = false;
          }
          if (itemPLaying.value!.isPlayed.value &&
              currentDuration.value < totalDuration.value) {
            currentDuration.value += 1;
          }
        }
      });
    }
  }

  String formatSecondsToMinutesAndSeconds(int totalSeconds) {
    final Duration duration = Duration(seconds: totalSeconds);
    final String formattedTime =
        "${duration.inMinutes}:${duration.inSeconds.remainder(60)}";
    return formattedTime;
  }

  Future<void> _loadSongs() async {
    final data = await rootBundle.loadString("lib/utils/songs.json");

    var jsonData = json.decode(data);

    songs.value =
        List.from(jsonData['songs']).map((e) => Song.fromJson(e)).toList();
  }

  @override
  void dispose() {
    timer!.cancel();
    super.dispose();
  }
}
