import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:music_app/utils/colors.dart';

class FixedSong extends StatelessWidget {
  String title;
  VoidCallback onPressedStop;

  FixedSong({Key? key, required this.title, required this.onPressedStop})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ClipRRect(
      borderRadius: BorderRadius.circular(6),
      child: BackdropFilter(
        filter: ImageFilter.blur(sigmaX: 28.0, sigmaY: 28.0),
        child: Container(
          padding: const EdgeInsets.all(7),
          decoration: BoxDecoration(
              gradient: LinearGradient(
                  colors: [
                    pinkColor.withOpacity(0.05),
                    backgroundColor.withOpacity(0.3),
                    blackColor
                  ],
                  begin: Alignment.centerLeft,
                  end: Alignment.centerRight,
                  stops: const [0.0, 0.5, 1.0]),
              borderRadius: BorderRadius.circular(6)),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Container(
                height: 42.0,
                width: 42.0,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(4),
                  image: const DecorationImage(
                    image: AssetImage('assets/icons/singer1.png'),
                    fit: BoxFit.cover,
                  ),
                ),
              ),
              const SizedBox(width: 10),
              Expanded(
                  child: Text(
                title,
                style: const TextStyle(
                    fontSize: 16,
                    color: whiteColor,
                    fontFamily: 'Lato',
                    letterSpacing: 0.1,
                    fontWeight: FontWeight.w300),
              )),
              Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  GestureDetector(
                    onTap: () {
                      onPressedStop();
                    },
                    child: const Icon(
                      Icons.pause,
                      size: 30,
                      color: whiteColor,
                    ),
                  ),
                  const SizedBox(width: 5),
                  const Icon(
                    Icons.skip_next,
                    size: 30,
                    color: whiteColor,
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
