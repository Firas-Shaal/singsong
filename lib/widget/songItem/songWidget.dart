import 'package:flutter/material.dart';
import 'package:music_app/model/songModel.dart';
import 'package:music_app/utils/colors.dart';

class SongItem extends StatelessWidget {
  Song song;
  dynamic onClickPlay;

  SongItem({
    Key? key,
    required this.song,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(bottom: 7),
      padding: const EdgeInsets.all(7),
      decoration: BoxDecoration(
          color: song.isPlayed.value ? whiteColor.withOpacity(0.15) : null,
          borderRadius: BorderRadius.circular(11),
          border: Border.all(style: BorderStyle.none)),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          GestureDetector(
            child: Container(
              padding: const EdgeInsets.all(5),
              decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  color: song.isPlayed.value ? pinkColor : whiteColor),
              child: Center(
                child: Icon(
                  song.isPlayed.value ? Icons.pause : Icons.play_arrow,
                  color: song.isPlayed.value ? whiteColor : pinkColor,
                ),
              ),
            ),
          ),
          Expanded(
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 10),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(song.title,
                      style: const TextStyle(
                          fontSize: 16,
                          color: whiteColor,
                          fontFamily: 'Poppins',
                          fontWeight: FontWeight.w700)),
                  const SizedBox(height: 5),
                  Text(song.singer,
                      style: const TextStyle(
                          fontSize: 13,
                          color: whiteColor,
                          fontFamily: 'Poppins',
                          fontWeight: FontWeight.w300)),
                ],
              ),
            ),
          ),
          Text(
            song.duration,
            style: const TextStyle(
                fontSize: 13,
                color: whiteColor,
                letterSpacing: 1.5,
                fontFamily: 'Poppins',
                fontWeight: FontWeight.w300),
          )
        ],
      ),
    );
  }
}
