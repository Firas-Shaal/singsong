import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:music_app/utils/colors.dart';
import 'package:music_app/widget/bottomBar/animateBar.dart';
import 'package:music_app/widget/bottomBar/menu.dart';

class BtmNavItem extends StatelessWidget {
  const BtmNavItem(
      {super.key,
        required this.navBar,
        required this.index,
        required this.press,
        required this.selectedNav});

  final Menu navBar;
  final int index;
  final VoidCallback press;
  final Menu selectedNav;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: index == 2 ? null : press,
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          Container(
            padding: index == 2 ? const EdgeInsets.all(15) : null,
            decoration: index == 2
                ? const BoxDecoration(
              color: pinkColor,
              shape: BoxShape.circle,
            )
                : null,
            child: SizedBox(
              height: selectedNav == navBar ? 25 : 20,
              width: selectedNav == navBar ? 25 : 20,
              child: SvgPicture.asset(
                navBar.rive,
                color: Colors.white,
              ),
            ),
          ),
          if (index != 2)
            AnimatedBar(
              isActive: selectedNav == navBar,
              // animation: animation,
            )
        ],
      ),
    );
  }
}
