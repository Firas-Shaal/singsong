class Menu {
  final String title;
  final String rive;

  Menu({required this.title, required this.rive});
}

List<Menu> bottomNavItems = [
  Menu(
    title: "Home",
    rive: 'assets/RiveAssets/home.svg',
  ),
  Menu(title: "Search", rive: 'assets/RiveAssets/search.svg'),
  Menu(title: "Headphone", rive: 'assets/RiveAssets/headphone.svg'),
  Menu(title: "Podcast", rive: 'assets/RiveAssets/podcast.svg'),
  Menu(title: "Settings", rive: 'assets/RiveAssets/settings.svg'),
];
