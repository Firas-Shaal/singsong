import 'package:flutter/material.dart';
import 'package:music_app/utils/colors.dart';

class AnimatedBar extends StatelessWidget {
  const AnimatedBar({
    Key? key,
    required this.isActive,
  }) : super(key: key);

  final bool isActive;

  @override
  Widget build(BuildContext context) {
    return AnimatedContainer(
      margin: isActive ? const EdgeInsets.only(top: 10) : null,
      duration: const Duration(milliseconds: 200),
      height: 4,
      width: isActive ? 20 : 0,
      decoration: const BoxDecoration(
          color: pinkColor,
          borderRadius: BorderRadius.all(
            Radius.circular(12),
          )),
    );
  }
}
