import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:music_app/controller/homeController.dart';
import 'package:music_app/screen/songScreen.dart';
import 'package:music_app/utils/colors.dart';
import 'package:music_app/widget/songItem/fixedSong.dart';
import 'package:music_app/widget/songItem/songWidget.dart';

class MusicScreen extends GetView<HomeController> {
  const MusicScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Get.put(HomeController());
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 7),
      child: Column(
        children: [
          const SizedBox(height: 60),
          const Padding(
            padding: EdgeInsets.symmetric(vertical: 10),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  'ALL SONGS',
                  style: TextStyle(
                      fontFamily: 'Poppins',
                      fontWeight: FontWeight.w700,
                      color: whiteColor,
                      fontSize: 18),
                ),
                Icon(
                  Icons.search,
                  color: whiteColor,
                )
              ],
            ),
          ),
          Obx(() => Expanded(
                child: Stack(
                  alignment: Alignment.bottomCenter,
                  children: [
                    SingleChildScrollView(
                      child: Column(
                        children: List<Widget>.generate(
                            controller.songs.length,
                            (index) => GestureDetector(
                                  key: Key(index.toString()),
                                  onTap: () {
                                    if (!controller
                                        .songs[index].isPlayed.value) {
                                      for (var element in controller.songs) {
                                        element.isPlayed.value = false;
                                      }
                                      controller.itemPLaying.value =
                                          controller.songs[index];
                                      controller.songs[index].isPlayed.value =
                                          true;
                                      if (controller.timer != null) {
                                        controller.timer!.cancel();
                                        controller.timer = null;
                                        controller.isInit();
                                      } else {
                                        controller.isInit();
                                      }
                                    }
                                    Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (context) => SingScreen(
                                              song: controller.songs[index])),
                                    ).then((value) => value == false
                                        ? controller.itemPLaying.value = null
                                        : () {});
                                  },
                                  child: SongItem(
                                    song: controller.songs[index],
                                  ),
                                )).followedBy([
                          SizedBox(
                              height: controller.itemPLaying.value == null
                                  ? 0
                                  : MediaQuery.of(context).size.height * 0.1)
                        ]).toList(),
                      ),
                    ),
                    if (controller.itemPLaying.value != null)
                      GestureDetector(
                        onTap: () {
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => SingScreen(
                                    song: controller.itemPLaying.value)),
                          );
                        },
                        child: FixedSong(
                            title: controller.itemPLaying.value!.title,
                            onPressedStop: () {
                              controller.timer!.cancel();
                              controller.timer = null;
                              for (var element in controller.songs) {
                                element.isPlayed.value = false;
                              }
                              controller.itemPLaying.value = null;
                            }),
                      )
                  ],
                ),
              ))
        ],
      ),
    );
  }
}
