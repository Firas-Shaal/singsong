import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:music_app/controller/homeController.dart';
import 'package:music_app/screen/songScreen.dart';
import 'package:music_app/utils/colors.dart';
import 'package:music_app/widget/songItem/fixedSong.dart';

class PodcastsScreen extends GetView<HomeController> {
  const PodcastsScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Get.put(HomeController());
    return Stack(
      alignment: Alignment.bottomCenter,
      children: [
        const Center(
            child: Text(
          'Podcasts',
          style: TextStyle(
              fontWeight: FontWeight.w700,
              fontFamily: 'Poppins',
              color: whiteColor,
              fontSize: 18),
        )),
        Obx(() => controller.itemPLaying.value == null
            ? const SizedBox()
            : GestureDetector(
                onTap: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) =>
                            SingScreen(song: controller.itemPLaying.value)),
                  );
                },
                child: FixedSong(
                    title: controller.itemPLaying.value!.title,
                    onPressedStop: () {
                      controller.timer!.cancel();
                      controller.timer = null;
                      for (var element in controller.songs) {
                        element.isPlayed.value = false;
                      }
                      controller.itemPLaying.value = null;
                    }),
              )),
      ],
    );
  }
}
