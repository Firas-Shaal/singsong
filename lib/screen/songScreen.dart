import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:music_app/controller/homeController.dart';
import 'package:music_app/model/songModel.dart';
import 'package:music_app/utils/colors.dart';

class SingScreen extends GetView<HomeController> {
  Song? song;

  SingScreen({Key? key, this.song}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Get.put(HomeController());
    return Obx(
      () => WillPopScope(
        onWillPop: () {
          if (controller.itemPLaying.value!.isPlayed.value == false) {
            Navigator.pop(context, false);
          } else {
            Navigator.pop(context, true);
          }
          return Future.value(false);
        },
        child: Scaffold(
          backgroundColor: backgroundColor,
          body: Stack(
            children: [
              Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(4),
                  image: const DecorationImage(
                    image: AssetImage('assets/icons/singer1.png'),
                    opacity: 0.4,
                    fit: BoxFit.cover,
                  ),
                ),
              ),
              BackdropFilter(
                filter: ImageFilter.blur(sigmaX: 70.0, sigmaY: 70.0),
                child: Column(
                  children: [
                    Expanded(
                      child: Padding(
                        padding: EdgeInsets.only(
                            top: MediaQuery.of(context).size.height * 0.065),
                        child: Row(
                          children: [
                            GestureDetector(
                              onTap: () {
                                if (controller
                                        .itemPLaying.value!.isPlayed.value ==
                                    false) {
                                  Navigator.pop(context, false);
                                } else {
                                  Navigator.pop(context, true);
                                }
                              },
                              child: const Padding(
                                padding: EdgeInsets.symmetric(horizontal: 15),
                                child: Icon(
                                  Icons.arrow_back_ios_new,
                                  color: whiteColor,
                                ),
                              ),
                            ),
                            const Expanded(
                              child: Text(
                                'Now PLAYING',
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                    fontWeight: FontWeight.w700,
                                    fontFamily: 'Poppins',
                                    color: whiteColor,
                                    fontSize: 17),
                              ),
                            ),
                            const Padding(
                              padding: EdgeInsets.symmetric(horizontal: 15),
                              child: Icon(
                                Icons.search,
                                color: whiteColor,
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                    Expanded(
                      flex: 3,
                      child: Stack(
                        alignment: Alignment.center,
                        children: [
                          SvgPicture.asset('assets/icons/wave.svg'),
                          Image.asset('assets/icons/person.png')
                        ],
                      ),
                    ),
                    Expanded(
                      child: Padding(
                        padding: const EdgeInsets.symmetric(
                            horizontal: 20, vertical: 20),
                        child: Row(
                          children: [
                            Expanded(
                              child: OutlinedButton(
                                style: OutlinedButton.styleFrom(
                                  foregroundColor: whiteColor,
                                  side: const BorderSide(
                                      color: whiteColor, width: 1.5),
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(40.0),
                                  ),
                                ),
                                onPressed: () {},
                                child: const Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Icon(Icons.favorite_border),
                                    SizedBox(width: 5),
                                    Text(
                                      'FOLLOW',
                                      style: TextStyle(
                                          fontSize: 13,
                                          color: whiteColor,
                                          fontFamily: 'Lato',
                                          fontWeight: FontWeight.w700),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                            const SizedBox(width: 10),
                            Expanded(
                              child: OutlinedButton(
                                style: OutlinedButton.styleFrom(
                                  backgroundColor: pinkColor,
                                  foregroundColor: whiteColor,
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(40.0),
                                  ),
                                ),
                                onPressed: () {},
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    SvgPicture.asset(
                                        'assets/icons/shuffle.svg'),
                                    const SizedBox(width: 5),
                                    const Text(
                                      'SHUFFLE PLAY',
                                      style: TextStyle(
                                          fontSize: 13,
                                          color: whiteColor,
                                          fontFamily: 'Lato',
                                          fontWeight: FontWeight.w700),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                    Expanded(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text(
                            song!.title,
                            style: const TextStyle(
                                fontSize: 19,
                                fontWeight: FontWeight.w700,
                                fontFamily: 'Poppins',
                                color: whiteColor),
                          ),
                          const SizedBox(height: 20),
                          Text(
                            song!.singer,
                            style: const TextStyle(
                                fontSize: 14,
                                fontWeight: FontWeight.w300,
                                fontFamily: 'Poppins',
                                color: whiteColor),
                          ),
                        ],
                      ),
                    ),
                    Expanded(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text(
                            controller.formatSecondsToMinutesAndSeconds(
                                controller.currentDuration.value),
                            style: const TextStyle(
                                fontSize: 14,
                                letterSpacing: 1.5,
                                fontWeight: FontWeight.w300,
                                fontFamily: 'Poppins',
                                color: whiteColor),
                          ),
                          SizedBox(
                            width: MediaQuery.of(context).size.width * 0.75,
                            child: Slider(
                              value: controller.currentDuration.value * 1.0,
                              onChanged: (value) {
                                controller.currentDuration.value =
                                    value.toInt();
                              },
                              activeColor: pinkColor,
                              inactiveColor: inactiveColor,
                              min: 0.0,
                              max: controller.totalDuration.value * 1.0,
                            ),
                          ),
                          Text(
                            controller.formatSecondsToMinutesAndSeconds(
                                controller.totalDuration.value),
                            style: const TextStyle(
                                fontSize: 14,
                                letterSpacing: 1.5,
                                fontWeight: FontWeight.w300,
                                fontFamily: 'Poppins',
                                color: whiteColor),
                          ),
                        ],
                      ),
                    ),
                    Expanded(
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          const Icon(Icons.skip_previous, color: whiteColor),
                          const Icon(Icons.fast_rewind, color: whiteColor),
                          GestureDetector(
                            onTap: () {
                              controller.itemPLaying.value!.isPlayed.value =
                                  !controller.itemPLaying.value!.isPlayed.value;
                            },
                            child: Container(
                              padding: const EdgeInsets.all(10),
                              decoration: const BoxDecoration(
                                  shape: BoxShape.circle, color: whiteColor),
                              child: Center(
                                child: Icon(
                                  controller.itemPLaying.value == null
                                      ? Icons.play_arrow
                                      : controller
                                              .itemPLaying.value!.isPlayed.value
                                          ? Icons.pause
                                          : Icons.play_arrow,
                                  size: 50,
                                ),
                              ),
                            ),
                          ),
                          const Icon(Icons.fast_forward, color: whiteColor),
                          const Icon(Icons.skip_next, color: whiteColor),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
