// ignore_for_file: use_key_in_widget_constructors, file_names, must_be_immutable

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:music_app/controller/homeController.dart';
import 'package:music_app/screen/musicScreen.dart';
import 'package:music_app/screen/podcastsScreen.dart';
import 'package:music_app/screen/searchScreen.dart';
import 'package:music_app/screen/settingsScreen.dart';
import 'package:music_app/utils/colors.dart';
import 'package:music_app/widget/bottomBar/menu.dart';
import 'package:music_app/widget/bottomBar/navItem.dart';

class HomePage extends GetView<HomeController> {
  Menu selectedBottomNav = bottomNavItems.first;

  void updateSelectedBtmNav(Menu menu) {
    if (selectedBottomNav != menu) {
      selectedBottomNav = menu;
      controller.selectedIndex.value =
          bottomNavItems.indexWhere((element) => element.title == menu.title);
    }
  }

  List<Widget> _buildScreens() {
    return [
      const MusicScreen(),
      const SearchScreen(),
      const SizedBox(),
      const PodcastsScreen(),
      const SettingsScreen(),
    ];
  }

  @override
  Widget build(BuildContext context) {
    Get.put(HomeController());
    return Obx(
      () => Scaffold(
        backgroundColor: backgroundColor,
        body: _buildScreens()[controller.selectedIndex.value],
        bottomNavigationBar: Container(
          padding: const EdgeInsets.symmetric(horizontal: 25, vertical: 10),
          decoration: const BoxDecoration(
            color: bottomBarColor,
            boxShadow: [
              BoxShadow(
                color: shadowColorLight,
                offset: Offset(0, -5),
                blurRadius: 25,
              ),
            ],
          ),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              ...List.generate(
                bottomNavItems.length,
                (index) {
                  Menu navBar = bottomNavItems[index];
                  return BtmNavItem(
                    navBar: navBar,
                    index: index,
                    press: () {
                      updateSelectedBtmNav(navBar);
                    },
                    selectedNav: selectedBottomNav,
                  );
                },
              ),
            ],
          ),
        ),
      ),
    );
  }
}
